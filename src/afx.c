#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libusb-1.0/libusb.h>

#define VID              0x187c
#define PID              0x0530

#define BYTE_FILL        0x00
#define BYTE_START       0x02
#define BYTE_COLOUR      0x03
#define BYTE_END_LOOP    0x04
#define BYTE_EXECUTE     0x05
#define BYTE_READY       0x06
#define BYTE_RESET       0x07
#define BYTE_BRIGHTNESS  0x1C
#define BYTE_DELAY       0x0E

#define STATUS_READY     0x10
#define STATUS_BUSY      0x11

#define MODE_BOOT        0x01

#define SIZE_WRITE       12
#define SIZE_READ        12

#define WRITE_TYPE       0x21
#define WRITE_REQUEST    0x09
#define WRITE_VALUE      0x202
#define WRITE_INDEX      0x00

#define READ_TYPE        0xa1
#define READ_REQUEST     0x01
#define READ_VALUE       0x101
#define READ_INDEX       0x0

typedef struct _usb_handle {
    libusb_context       *usb_context;
    libusb_device_handle *usb_handle;
} usb_handle;

struct timespec ts1 = {
    .tv_sec = 0,
    .tv_nsec = 5 * 1000 * 1000
};

unsigned int limit_uint(unsigned int min, int n, unsigned int max)
{
    return ((n <= min) ? min : ((n >= max) ? max : n));
}

int init_device(usb_handle *uh)
{
    if (libusb_init(&uh->usb_context) == 0) {
        libusb_set_debug(uh->usb_context, 3);

        if ((uh->usb_handle = libusb_open_device_with_vid_pid(uh->usb_context,
                              VID, PID))) {
            if (uh->usb_handle) {
                if (1 == libusb_kernel_driver_active(uh->usb_handle, 0))
                    (void)libusb_detach_kernel_driver(uh->usb_handle, 0);

                if (0 > libusb_claim_interface(uh->usb_handle, 0)) {
                    printf("Failed claim interface\n");
                    uh->usb_handle = 0;
                    return -1;
                }
                return 1;
            }
        }
        printf("Failed to open device");
        return -1;
    } else {
        printf("Failed to init libusb");
        return -1;
    }

    return 0;
}

void close_device(usb_handle *uh)
{
    libusb_release_interface(uh->usb_handle, 0);
    libusb_attach_kernel_driver(uh->usb_handle, 0);
    libusb_close(uh->usb_handle);
    libusb_exit(uh->usb_context);
}

void device_write(usb_handle *uh, unsigned char *command, int size)
{
    unsigned char data[SIZE_WRITE];
    memset(&data[0], BYTE_FILL, SIZE_WRITE);
    memcpy(&data[0], command, size);

    int ret = libusb_control_transfer(uh->usb_handle, WRITE_TYPE,
                                      WRITE_REQUEST, WRITE_VALUE, WRITE_INDEX,
                                      data, sizeof data, 0);

    if (ret != SIZE_WRITE)
        printf("Failed device write (%d bytes)\n", ret);

    nanosleep(&ts1, NULL);
}

int device_read(usb_handle *uh, unsigned char *data, int size)
{
    unsigned char buffer[SIZE_READ];
    memset(&buffer[0], BYTE_FILL, SIZE_READ);

    int ret = libusb_control_transfer(uh->usb_handle, READ_TYPE, READ_REQUEST,
                                      READ_VALUE, READ_INDEX, &buffer[0],
                                      sizeof buffer, 0);

    if (ret <= size) {
        memcpy(data, &buffer[0], size);
        if (ret < size)
            memset(&data[size], '\0', size - (ret < 0 ? 0 : size));
    }
    return ret;
}

void get_ready(usb_handle *uh)
{
    unsigned char command[] = {BYTE_START, BYTE_READY};
    device_write(uh, &command[0], sizeof command);

    char ready = 0;
    unsigned char status[SIZE_READ];
    while (ready == 0) {
        int ret = device_read(uh, &status[0], SIZE_READ);
        if (0 < ret) {
            if (status[0] == STATUS_READY || status[0] == STATUS_BUSY)
                ready = 1;
        } else if (LIBUSB_ERROR_BUSY == ret) {
            printf("Libusb error busy\n");
            nanosleep(&ts1, NULL);
            close_device(uh);
            exit(-1);
        } else {
            nanosleep(&ts1, NULL);
        }
    }
}

void execute(usb_handle *uh)
{
    unsigned char command[] = {BYTE_START, BYTE_EXECUTE};
    device_write(uh, &command[0], sizeof command);
}

void reset(usb_handle *uh)
{
    unsigned char command[] = {BYTE_START, BYTE_RESET, 0x04};
    device_write(uh, &command[0], sizeof command);
}

void set_delay(usb_handle *uh)
{
    unsigned char command[] = {BYTE_START, BYTE_DELAY, BYTE_FILL, 0x64, 0xB8};
    device_write(uh, &command[0], sizeof command);
}

void end_loop(usb_handle *uh)
{
    unsigned char command[] = {BYTE_START, BYTE_END_LOOP};
    device_write(uh, &command[0], sizeof command);
}

void set_brightness(usb_handle *uh, unsigned int brightness)
{
    unsigned int b_int = brightness * 10;
    unsigned char b_char = BYTE_FILL;
    memcpy(&b_char, &b_int, sizeof(unsigned char));
    unsigned char command[] = {BYTE_START, BYTE_BRIGHTNESS, b_char, MODE_BOOT};
    device_write(uh, &command[0], sizeof command);
}

void set_colours(usb_handle *uh, int zone, unsigned int *colours, int size)
{
    unsigned char c[3] = {BYTE_FILL, BYTE_FILL, BYTE_FILL};
    memcpy(&c[0], &colours[0], sizeof(unsigned char));
    memcpy(&c[1], &colours[1], sizeof(unsigned char));
    memcpy(&c[2], &colours[2], sizeof(unsigned char));

    unsigned char r[3] = {0x00, 0x3C, 0xEF};

    unsigned char command[] = {BYTE_START, BYTE_COLOUR, MODE_BOOT,
                               r[0], r[1], r[2], c[0], c[1], c[2]
                              };
    device_write(uh, &command[0], sizeof command);
}

int main(int argc, char **argv)
{
    usb_handle uh;

    if (init_device(&uh) <= 0) {
        close_device(&uh);
        return -1;
    }

    get_ready(&uh);

    switch (argc) {
    case 3:
        if (strcmp(argv[1], "brightness") == 0) {
            unsigned int brightness = 0;
            sscanf(argv[2], "%d", &brightness);

            brightness = limit_uint(0, brightness, 10);

            set_delay(&uh);
            set_brightness(&uh, brightness);
        }
        break;
    case 6:
        // Permanent colour setting
        if (strcmp(argv[1], "colour") == 0) {
            int zone = 0;
            unsigned int colours[3] = {0, 0, 0};

            sscanf(argv[2], "%d", &zone);

            sscanf(argv[3], "%d", &colours[0]);
            sscanf(argv[4], "%d", &colours[1]);
            sscanf(argv[5], "%d", &colours[2]);

            colours[0] = limit_uint(0, colours[0], 255);
            colours[1] = limit_uint(0, colours[1], 255);
            colours[2] = limit_uint(0, colours[2], 255);

            reset(&uh);
            get_ready(&uh);
            set_delay(&uh);
            set_colours(&uh, zone, colours, sizeof colours);
            end_loop(&uh);
            execute(&uh);
        }
        // Loop colours in the order added
        else if (strcmp(argv[1], "cycle") == 0) {
            int zone = 0;
            unsigned int colours[3] = {0, 0, 0};

            sscanf(argv[2], "%d", &zone);

            sscanf(argv[3], "%d", &colours[0]);
            sscanf(argv[4], "%d", &colours[1]);
            sscanf(argv[5], "%d", &colours[2]);

            colours[0] = limit_uint(0, colours[0], 255);
            colours[1] = limit_uint(0, colours[1], 255);
            colours[2] = limit_uint(0, colours[2], 255);

            set_delay(&uh);
            set_colours(&uh, zone, colours, sizeof colours);
            end_loop(&uh);
            execute(&uh);
        }
        break;

    default:
        printf("HELP\n");
        break;
    }

    close_device(&uh);
}
