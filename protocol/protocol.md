https://github.com/Xqua/pyAlienFX/blob/master/AlienFX/AlienFXComputers.py
https://github.com/jrobeson/pyalienfx/tree/master/AlienFX



* Generic parts

    All messages are length 12 and are padded with fill bytes if needed.

    Start byte: 0x02
    Fill byte: 0x00
    Mode byte:   - boot:     0x01
                 - standy:   0x02
                 - ac power: 0x05
                 - charging: 0x06
                 - battery:  0x08

* Set morph ?

    command: 0x01

* Set blink ?

    command: 0x02

* Colour message

    [start] [command] [mode] [region] [region] [region] [red] [green] [blue]

    command: 0x03
    region: 0x00 till 0xFF
    colour: 0x00 till 0xFF

    default reg: 0x00 0x3C 0xEF

* Loop block end ?

    [start] [command]

    command: 0x04

* Transmit and execute

    [start] [command]

    command: 0x05

* Get status

    [start] [command]

    command: 0x06

    Seems to return either 0x10 post-reset or 0x11 pre-reset.

* Reset

    [start] [command] [0x04]

    command: 0x07

* Set speed ?

    [start] [command] [0x00] [0x64] [0xB8]

    command: 0x0E 

* Brightness

    [start] [command] [brightness] [mode]

    command: 0x1C
    brightness: 0x00 till 0x64

